package pe.gob.vuce.zee.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VehiculoDTO {

	private Long idvehiculo;	

	private String placa;

	private String marca;

	private String modelo;

	private String color;

	private Integer anio;

	private String estado;

	
	//Constructor para la lista de vehiculos
	public VehiculoDTO(Long idvehiculo, String placa, String marca, String modelo, String color, Integer anio,
			String estado) {
		super();
		this.idvehiculo = idvehiculo;
		this.placa = placa;
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		this.anio = anio;
		this.estado = estado;
	}	
	
	
	
	
	
	

}

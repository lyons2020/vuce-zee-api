package pe.gob.vuce.zee.utils;

public class ConstantesUtil {

	public static final String FORMATO_FECHA_CORTO = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_RAYA_CORTO = "dd-MM-yyyy";
	public static final String FORMATO_FECHA_LARGO = "dd/MM/yyyy HH:mm:ss";

	public static final String IND_ACTIVO = "1";
	public static final String IND_INACTIVO = "0";
	
	
}

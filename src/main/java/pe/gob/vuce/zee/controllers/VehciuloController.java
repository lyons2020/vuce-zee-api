package pe.gob.vuce.zee.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import pe.gob.vuce.zee.dtos.FiltroBusquedaDTO;
import pe.gob.vuce.zee.dtos.VehiculoDTO;
import pe.gob.vuce.zee.services.VehiculoService;

@Slf4j
@RestController
@RequestMapping("/v1/vehiculos")
public class VehciuloController {
	
	
	@Autowired
	private VehiculoService vehiculoService;
	
	
	
	@PostMapping("/filtrar")
	public ResponseEntity<Page<VehiculoDTO>> listarCursosByFiltro(@RequestBody FiltroBusquedaDTO filtroBusquedaDTO, 
			Pageable paginador) {
		
	//	log.info("ingresando a filtrar vehiculos");

		Page<VehiculoDTO> listaDTO = this.vehiculoService.listarVehiculosByFiltro(filtroBusquedaDTO,
				paginador);

		return new ResponseEntity<Page<VehiculoDTO>>(listaDTO, HttpStatus.OK);
	}
	
	
	
	

}

package pe.gob.vuce.zee.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenApiConfig {


	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.components(new Components())
				.info(new Info()
						.title("VUCE API-ZEE")
						.description("Sistema Integrado de Gesti�n de Operaciones de las Zonas Econ�micas Especiales - ZEE")	     
						//.termsOfService("terms")
						.contact(new Contact().email("info@vuce.gob.pe"))
						.license(new License().name("Apche 2.0"))
						.version("1.0.0")
						);
	}



	
	/* manera dinamica desde el yml
	 
	application-description=@project.description@			
	application-version=@project.version@
	
	@Bean	 
	public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption, 
			@Value("${application-version}") String appVersion) {

		return new OpenAPI()
				.info(new Info()
						.title("sample application API")
						.version(appVersion)
						.description(appDesciption)
						.termsOfService("http://swagger.io/terms/")
						.license(new License().name("Apache 2.0").url("http://springdoc.org"))
				);

	}*/



}

package pe.gob.vuce.zee.base;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;


@MappedSuperclass
@Data
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)	
	private Date fecregistro;	
	
	private Long usuarioregistro;		
	
	@Temporal(TemporalType.TIMESTAMP)	
	private Date fecmodifica;
	
	private Long usuariomodifica;	
	
	private String ipregistro;
	
	private String ipmodifica;	
	
	private String operacion; //1 INSERT y 2 UPDATE	

	
	
}

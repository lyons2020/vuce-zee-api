package pe.gob.vuce.zee.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import pe.gob.vuce.zee.dtos.FiltroBusquedaDTO;
import pe.gob.vuce.zee.dtos.VehiculoDTO;
import pe.gob.vuce.zee.models.repository.VehiculoRepository;
import pe.gob.vuce.zee.services.VehiculoService;

@Slf4j
@Service
@Transactional(readOnly = true)
public class VehiculoServiceImpl implements VehiculoService {	
	
	@Autowired	
	private VehiculoRepository vehiculoRepository;	
	
	

	@Override
	public Page<VehiculoDTO> listarVehiculosByFiltro(FiltroBusquedaDTO filtroDTO, Pageable paginador) {		
		//log.info("ingresando a listar vehiculos by filtro"+filtroDTO.getTextoBusqueda()); 
		return this.vehiculoRepository.listarVehiculosByFiltro(filtroDTO.getTextoBusqueda(), paginador);
	}
	

}

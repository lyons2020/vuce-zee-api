package pe.gob.vuce.zee.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import pe.gob.vuce.zee.dtos.FiltroBusquedaDTO;
import pe.gob.vuce.zee.dtos.VehiculoDTO;

public interface VehiculoService {
	
	
	Page<VehiculoDTO> listarVehiculosByFiltro(FiltroBusquedaDTO filtroDTO, Pageable paginador);

}

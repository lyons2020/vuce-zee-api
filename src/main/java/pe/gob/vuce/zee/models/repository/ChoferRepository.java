package pe.gob.vuce.zee.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.vuce.zee.models.entity.Chofer;

public interface ChoferRepository extends JpaRepository<Chofer, Long>{

}

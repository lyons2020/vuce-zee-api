package pe.gob.vuce.zee.models.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pe.gob.vuce.zee.dtos.VehiculoDTO;
import pe.gob.vuce.zee.models.entity.Vehiculo;

public interface VehiculoRepository extends JpaRepository<Vehiculo, Long> {
	
	
	@Query("SELECT new pe.gob.vuce.zee.dtos.VehiculoDTO( "				
			+ "v.idvehiculo as idvehiculo, v.placa as placa, v.marca as marca, v.modelo as modelo,"
			+ "v.color as color, v.anio as anio, v.estado ) "			
			+ "FROM Vehiculo v "			
			+ "WHERE v.estado = '1' "	)			
			//+ "AND (:textoBusqueda IS NULL OR ( "
			//+ "v.placa LIKE CONCAT('%', :textoBusqueda, '%') ) )"	)	
			//+ "OR v.marca LIKE CONCAT('%', :textoBusqueda, '%') "
			//+ "OR v.modelo LIKE CONCAT('%', :textoBusqueda, '%') "
			//+ "OR v.color LIKE CONCAT('%', :textoBusqueda, '%') "
			//+ "OR v.anio LIKE CONCAT('%', :textoBusqueda, '%') ) ) ")	
	Page<VehiculoDTO> listarVehiculosByFiltro(@Param("textoBusqueda") String textoBusqueda, Pageable paginador);

}

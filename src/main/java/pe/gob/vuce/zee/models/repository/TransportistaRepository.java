package pe.gob.vuce.zee.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.gob.vuce.zee.models.entity.Transportista;

public interface TransportistaRepository extends JpaRepository<Transportista, Long> {

}

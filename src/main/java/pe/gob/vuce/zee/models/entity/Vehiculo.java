package pe.gob.vuce.zee.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pe.gob.vuce.zee.base.BaseEntity;



/**
* Clase Entidad para la tabla VEHICULO: 
* @descripci�n: 
*
* @author: elozano
* @version 1.0
* @fecha_de_creaci�n: 10/09/2021
*/

@Entity
@Table(name="VEHICULO")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Vehiculo extends BaseEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idvehiculo;	
	
	private String placa;

	private String marca;
	
	private String modelo;
	
	private String color;

	private Integer anio;

	private String estado;	

}





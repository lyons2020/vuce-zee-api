package pe.gob.vuce.zee.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pe.gob.vuce.zee.base.BaseEntity;


/**
* Clase Entidad para la tabla TRANSPORTISTA: 
* @descripci�n: 
*
* @author: elozano
* @version 1.0
* @fecha_de_creaci�n: 10/09/2021
*/

@Entity
@Table(name="TRANSPORTISTA")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Transportista extends BaseEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idtransportista;	
	
	private String razonsocial;

	private String direccion;

	private Integer telefono;

	private String estado;	
	

}

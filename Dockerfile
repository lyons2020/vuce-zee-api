#### EJECUCION DEL JAR ####
FROM openjdk:11
#LABEL maintainer="eulopa708@gmail.com"
# ENV DOCKERIZE_VERSION v0.6.1
# RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
#     && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
#     && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY build/libs/vuce-*.jar /zee-api.jar
#COPY --from=builder /app/target/apiRegion*.jar /app.jar

ENV host="postgres_server"
ENV port="5432"
ENV database="bdadmin"
ENV username="postgres"
ENV password="admin"

CMD ["-jar", "/zee-api.jar"]
ENTRYPOINT ["java"]
EXPOSE 8081